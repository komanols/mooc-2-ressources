- Thématique : Création d'un site internet

- Notions liées : Pages web, langage HTML.
- Résumé de l’activité : Création des pages web en utilisant le langage HTML.

- Objectifs : Cette activité vise à créer un site web statique en utilisant le langage HTML. 

- Auteur : Olivier KOMBO

- Durée de l’activité : 2 Heures

- Forme de participation : En binôme 

- Matériel nécessaire : Ordinateur

- Préparation : Connaissance des balises HTML

- Autres références : Notions sur les balises HTML