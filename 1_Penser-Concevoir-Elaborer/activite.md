- Thématique : Construction d'un site internet
Les sites web sont devenus incontournables dans plusieurs domaines. Ils permettent aux différents acteurs de la société de s'afficher, de proposer leurs services...

- Idée directrice: L'internet est basé essentiellement sur des sites web avec lesquels nous recherchons des informations, nous proposons des sevices, nous interagissons de différentes manières.

- Idée fondamentale: Les sites web sont des vitrines qui nous permettent d'interagir avec d'autres acteurs et services au niveau mondial.

- Objectif de disposition: Les étudiants ont pratiquement tous une idée de site internet et ont déjà consulté quelques sites pour plusieurs raisons.

- Objectifs pédagogiques opérationnels: Les étudiants peuvent citer les différents types des sites qui existent et même tenter de les classifier selon plusieurs critères.

-Objectifs pédagogiques: Rappeler le rôle de quelques balises HTML, donner la structure d'une page web, Construire des pages web

- Connaissance préalable: Connaissances des balises HTML

- Gestion du temps: Rappeler brièvement les balises HTML et expliquer la structure d'une page web puis laisser les élèves construire des pages web sur l'ordinateur


